import React from 'react';
import { BrowserRouter, Route, Switch, Link } from 'react-router-dom'


import PrimeraVista from './components/PrimeraVista/PrimeraVista.js'
import Resultados from './components/Resultados/Resultados';
import ProxPartidos from './components/ProxPartidos/ProxPartidos';

import Organizador from './components/Organizador/Organizador';
import AgregarEquipo from './components/Organizador/AgregarEquipo';
import VincularJugadores from './components/Organizador/VincularJugadores.js';
import OrganizarEncuentros from './components/Organizador/OrganizarEncuentros';
import AgregarDeporte from './components/Organizador/AgregarDeporte';

import AcercaDe from './components/AcercaDe/AcercaDe';





function App() {

  return (

    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={PrimeraVista} />
        
        <Route exact path="/resultados" component={Resultados} />
        
        <Route exact path="/proxpartidos" component={ProxPartidos} />
        
        <Route exact path="/organizador" component={Organizador} />
        <Route exact path="/agregarequipo" component={AgregarEquipo} />
        <Route exact path="/vincularjugadores" component={VincularJugadores} />
        <Route exact path="/organizarencuentros" component={OrganizarEncuentros} />
        <Route exact path="/agregardeporte" component={AgregarDeporte} />

        <Route exact path="/acercade" component={AcercaDe} />
       
        

      </Switch>

    </BrowserRouter>

  )

}



export default App;