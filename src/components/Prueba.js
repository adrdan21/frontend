import React, { Component, useEffect } from 'react';
import { AlertProvider } from 'react-alerts-plus';
import swal from 'sweetalert'



class Formulario extends Component {

    constructor(argumentos) {
        super(argumentos);

        this.state = {
            identificacion: "", tipo_doc: "", nombre1: "", nombre2: "",
            apellido1: "", apellido2: "", email: "", celular: "", fecha_nac: "", genero: "", username: "", password: "", message: ""
        }

        this.onChange = this.onChange.bind(this);
        this.enviarDatos = this.enviarDatos.bind(this);

    }

    onChange(evento) {

        let name = evento.target.name;
        let value = evento.target.value;

        this.setState({
            [name]: value, //podría poner cada uno de las variables nombre1, nombre2, etc pero con esto me ahorro código
        });

        console.log(this.state);

    }

    enviarDatos(evento) {

        evento.preventDefault();

    }

    save(evento) {

        if(!this.estoyValidando()){ //si se niega validación, es porque hay un error, entonces se devuelve
            return;
        }

        this.setState({
            message: 'Guardado correctamente'
        })
    }

    estoyValidando(){
        if(this.state.acept != true){
            this.setState({
                //message: 'Acepte los términos y condiciones'
            })
            return false
        }
        return true


    }

    render() {


        return (


            <div className="container bg-success">
                <form action="" onSubmit={this.enviarDatos}>

                    <div className="form-col">

                        <div className="form-group ">
                            <p >REGISTRO DE USUARIOS</p>
                        </div>

                        <div className="form-group ">
                            <input name="identificacion" id="identificacion" value={this.state.identificacion} onChange={this.onChange}
                                className="form-control form-control-sm" type="number" placeholder="Identificación" required />
                        </div>

                        <div className="form-group ">
                            <label htmlFor="tipo_doc" className="text-light">Tipo de Documento</label>
                            <select name="tipo_doc" id="tipo_idoc" value={this.state.tipo_doc} onChange={this.onChange}
                                className="selectpicker form-control form-control-sm" data-live-search="true" required  >
                                <option data-tokens="">--select--</option>
                                <option data-tokens="masc"> CC </option>
                                <option data-tokens="fem"> TI </option>
                                <option data-tokens="fem"> CE </option>
                            </select>
                        </div>



                        <div className="form-group ">
                            <input name="nombre1" id="nombre1" value={this.state.nombre1} onChange={this.onChange}
                                className="form-control form-control-sm" type="text" placeholder="Nombre 1" required />
                        </div>

                        <div className="form-group ">
                            <input name="nombre2" id="nombre2" value={this.state.nombre2} onChange={this.onChange}
                                className="form-control form-control-sm" type="text" placeholder="Nombre 2" />
                        </div>

                        <div className="form-group ">
                            <input name="apellido1" id="apellido1" value={this.state.apellido1} onChange={this.onChange}
                                className="form-control form-control-sm" type="text" placeholder="Apellido 1" required />
                        </div>

                        <div className="form-group ">
                            <input name="apellido2" id="apellido2" value={this.state.apellido2} onChange={this.onChange}
                                className="form-control form-control-sm" type="text" placeholder="Apellido 2" />
                        </div>

                        <div className="form-group ">
                            <input name="email" id="email" value={this.state.email} onChange={this.onChange}
                                className="form-control form-control-sm" type="text" placeholder="e-mail" required />
                        </div>

                        <div className="form-group ">
                            <input name="celular" id="movil" value={this.state.celular} onChange={this.onChange}
                                className="form-control form-control-sm" type="number" placeholder="Celular" />
                        </div>

                        <div className="form-group ">
                            <label htmlFor="fecha_nac" className="text-light">Fecha de Nacimiento</label>
                            <input name="fecha_nac" id="fecha_nac" value={this.state.fecha_nac} onChange={this.onChange}
                                className="form-control form-control-sm" type="date" placeholder="Fecha Nacimiento" />
                        </div>

                        <div className="form-group form-check text-light">

                            <div htmlFor="genero"  > Género </div>
                            <div className="m-1"><input name="genero" type="radio" value="masculino" className="form-check-input" onChange={this.onChange} />   Hombre </div>
                            <div className="m-1"><input name="genero" type="radio" value="femenino" className="form-check-input" onChange={this.onChange} />   Mujer </div>

                        </div>

                        <div className="form-group ">
                            <input name="username" id="username" value={this.state.username} onChange={this.onChange}
                                className="form-control form-control-sm" type="text" placeholder="Nombre de Usuario" required />

                        </div>

                        <div className="form-group ">
                            <input name="password" id="password" value={this.state.password} onChange={this.onChange}
                                className="form-control form-control-sm" type="password" placeholder="password" required />
                        </div>

                        

                        <div className="form-group ">
                            <input type="submit" value="Enviar" className="btn btn-success" onClick={this.save.bind(this)} />
                            <span style={{color:'white'}}>{this.state.message}</span>

                        </div>

                    </div>

                </form>

            </div >
        );
    }

}

export default Formulario;