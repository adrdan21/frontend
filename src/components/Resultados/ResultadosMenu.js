import React from 'react';





const ResultadosMenu = () => {


  return (
    
    
   
    <nav >
 
    <div className="menu-toggle">
        <h3>Menu</h3>
        <button type="button" id="menu-btn">
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
        </button>
    </div>
   
    
    <ul id="respMenu" className="ace-responsive-menu" data-menu-style="vertical">
        <li>
            <a href="javascript:;">
                <i className="fa fa-home" aria-hidden="true"></i>
                <span className="title">Microfutbol</span>
            </a>
        </li>
        <li>
            <a href="javascript:;">
                <i className="fa fa-cube" aria-hidden="true"></i>
                <span className="title">Ajedrez</span>

            </a>
            
            
        </li>

       
        <li>
            <a className="" href="javascript:;">
                <i className="fa fa-graduation-cap" aria-hidden="true"></i>
                <span className="title">Atletismo</span>

            </a>
     
        </li>
        <li>
            <a href="javascript:;">
                <i className="fa fa-heart" aria-hidden="true"></i>
                <span className="title">Tenis de mesa</span>
            </a>
        </li>
        <li className="last ">
            <a href="javascript:;">
                <i className="fa fa-envelope" aria-hidden="true"></i>
                <span className="title">Otros deportes</span>
            </a>
        </li>
    </ul>
</nav>

    
  );
}

export default ResultadosMenu;