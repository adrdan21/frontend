import React from 'react';
import Encabezado from "../Encabezado/Encabezado.js";
import ResultadosCuerpo from './ResultadosCuerpo';

function Resultados() {
  return (

    <div className="container ">
      <Encabezado />
      <ResultadosCuerpo />
    </div>

  );
}

export default Resultados;