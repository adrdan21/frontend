import React from 'react';
import Encabezado from "../Encabezado/Encabezado";
import OrganizadorCuerpo from './OrganizadorCuerpo';

function Organizador() {
  return (

    <div className="container ">
      <Encabezado />
      <OrganizadorCuerpo/>
    </div>

  );
}

export default Organizador;