
import React, { Component } from 'react';
import axios from 'axios';
import swal from 'sweetalert'
import { MDBContainer, MDBInput } from "mdbreact";
import { Link } from 'react-router-dom';
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import TimeField from 'react-simple-timefield';
import img_stilo from '../css/img_stilo/hora.css'

class AgregarDeporte extends Component {
   constructor(props) {
        super(props);
        this.state = {
            cod_deporte: '', max_integrantes: '', nombre_deporte: ''
        };
        this.handleChange = this.handleChange.bind(this);
    }
    handleChange = (e) => { this.setState({ [e.target.id]: e.target.value }) };

    handleSubmit = (e) => {

        e.preventDefault();
        console.log(this.state);
        let form_data = new FormData();

        form_data.append('cod_deporte', this.state.cod_deporte);
        form_data.append('max_integrantes', this.state.max_integrantes);
        form_data.append('nombre_deporte', this.state.nombre_deporte);

        let url = 'http://localhost:8000/registro/reg_deportes/';
        axios.post(url, form_data, {
            headers: {
                'content-type': 'multipart/form-data'
            }
        })
        .then(res => {
          console.log(res.data);
          swal({ 
            title: "Sistema",
            text: "Deporte almacenado satisfactoriamente",
            icon: "success",
            timer: "3000"
            })
        })
        .catch(err => console.log(err))
   };

 render() {
  return (
    <div className=' col bg-info text-white d-flex flex-column align-items-center'>
      <div className='row m-5'>
        <h1> Agregar Deporte</h1>
      </div>
      <div className='row m-2'>
        <form onSubmit={this.handleSubmit}>    
          <div className='form-group '>
          <label for='nombre_deporte'>Nombre Deporte</label>
            <input type='text' className='form-control' id='nombre_deporte' value={this.state.nombre_deporte} 
            onChange={this.handleChange} placeholder='Nombre Deporte' required />
          </div>
          <div className='form-group '>
          <label for='max_integrantes'>Máximo integrantes</label>
            <input type='num' className='form-control' id='max_integrantes' value={this.state.max_integrantes} 
            onChange={this.handleChange} placeholder='Máximo de integrantes' required />
          </div>
          <div className='form-group '>
          <label for='cod_deporte'>Código Deporte</label>
            <input type='text' className='form-control' id='cod_deporte' value={this.state.cod_deporte} 
            onChange={this.handleChange}
            placeholder='Código Deporte' required />
          </div>
          <input type='submit' className='btn btn-primary' />
        </form>
      </div>

      <div className='row '>
        <div className='col'>

          <ul className='nav'>
            <li><Link to='Organizador'> Volver</Link></li>
          </ul>

        </div>
      </div>

    </div>
  );
 }
  
}

export default AgregarDeporte;