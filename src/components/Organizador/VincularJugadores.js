import React, { Component } from 'react';
import axios from 'axios';
import swal from 'sweetalert'
import { Link } from 'react-router-dom';
import { BrowserRouter, Route, Switch } from 'react-router-dom'

class VincularJugadores extends Component {
    componentDidMount(){
        //Lista Programas
        axios
            .get('http://localhost:8000/registro/listar_usuarios/')
            .then((response) => { 
                console.log(response);
            this.setState({ personas: response.data})
            })
            .catch(error => {
                console.log(error);
            });
        //Programas
        axios
            .get('http://localhost:8000/listar/listar_programas/')
            .then((response) => { 
                console.log(response);
            this.setState({ programas: response.data})
            })
            .catch(error => {
                console.log(error);
            });
        //Semestres
        axios
            .get('http://localhost:8000/listar/listar_semestres/')
            .then((response) => { 
                console.log(response);
            this.setState({ semestres: response.data})
            })
            .catch(error => {
                console.log(error);
            });
        //Equipos
        axios
            .get('http://localhost:8000/registro/reg_equipos/')
            .then((response) => { 
                console.log(response);
            this.setState({ equipos: response.data})
            })
            .catch(error => {
                console.log(error);
            });
    }

    constructor(props) {
        super(props);
        this.state = {
            personas:[], programas:[], semestres:[], equipos:[]
        };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange = (e) => { this.setState({ [e.target.id]: e.target.value })};

    handleChangeEstudiante = (e) => { this.setState({ persona: e.target.value }) 
      console.log(e.target)
    };
    handleChangePrograma = (e) => { this.setState({ programa: e.target.value }) 
      console.log(e.target)
    };
    handleChangeSemestre = (e) => { this.setState({ semestre: e.target.value }) 
      console.log(e.target)
    };
    handleChangeEquipo = (e) => { this.setState({ equipo: e.target.value }) 
      console.log(e.target)
    };

    handleSubmit = (e) => {
        e.preventDefault();
        let form_data = new FormData();

        form_data.append('persona', this.state.persona);
        form_data.append('programa', this.state.programa);
        form_data.append('semestre', this.state.semestre);
        form_data.append('equipo', this.state.equipo);
        form_data.append('estado', 'activo');
        
        let url = 'http://localhost:8000/registro/vincular_jugadores/';
        axios.post(url, form_data, {
            headers: {
                'content-type': 'multipart/form-data'
            }
        })
        .then(res => {
          console.log(res.data);
          swal({ 
            title: "Sistema",
            text: "Jugador almacenado satisfactoriamente",
            icon: "success",
            timer: "3000"
            })
        })
        .catch(err => console.log(err))
    };

    render() {
        return (
            <div class=" col bg-info text-white d-flex flex-column align-items-center">
                <div class="row">
                    <h1>Vincular Jugadores</h1>
                </div>
                <div class="row m-4">
                    <form onSubmit={this.handleSubmit}>
                        <div class="form-group ">
                            <label for="persona">Estudiante</label>
                            <select id="persona" name="persona" value={this.state.value} 
                                onChange={this.handleChangeEstudiante}
                                className="selectpicker form-control form-control-sm" data-live-search="true">
                                <option></option>
                                { this.state.personas.map(elemento =>(
                                <option key={elemento.id} value={elemento.id}> 
                                {elemento.persona.identificacion} {elemento.persona.nombre1} {elemento.persona.apellido1} </option>
                                ))}
                            </select>
                        </div>
                        <div class="form-group ">
                            <label for="programa">Programa</label>
                            <select id="programa" name="programa" value={this.state.value} 
                                onChange={this.handleChangePrograma}
                                className="selectpicker form-control form-control-sm" data-live-search="true">
                                <option></option>
                                { this.state.programas.map(elemento =>(
                                <option key={elemento.id} value={elemento.id}> 
                                {elemento.nombre_programa}</option>
                                ))}
                            </select>
                        </div>
                        <div class="form-group ">
                            <label for="semestre">Semestre</label>
                            <select id="semestre" name="semestre" value={this.state.value} 
                                onChange={this.handleChangeSemestre}
                                className="selectpicker form-control form-control-sm" data-live-search="true">
                                <option></option>
                                { this.state.semestres.map(elemento =>(
                                <option key={elemento.id} value={elemento.id}> 
                                {elemento.nombre_semestre}</option>
                                ))}
                            </select>
                        </div>
                        <div class="form-group ">
                            <label for="equipo">Equipo</label>
                            <select id="equipo" name="equipo" value={this.state.value} 
                                onChange={this.handleChangeEquipo}
                                className="selectpicker form-control form-control-sm" data-live-search="true">
                                <option></option>
                                { this.state.equipos.map(elemento =>(
                                <option key={elemento.id} value={elemento.id}> 
                                {elemento.nombre_equipo}</option>
                                ))}
                            </select>
                        </div>

                        <input type="text" id="estado" name="estado" value="Activo" hidden />

                        <button type="submit" class="btn btn-success">Registrar</button>
                    </form>
                </div>
                <li class="btn btn-primary"><Link to="Organizador" style={{color: 'white'}}> Cancelar</Link></li>
            </div>
            
        );
    }
}

export default VincularJugadores;

