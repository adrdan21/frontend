import React, { Component, useState, useEffect } from 'react';
import axios from 'axios';
import swal from 'sweetalert'
import { Link } from 'react-router-dom';
import { BrowserRouter, Route, Switch } from 'react-router-dom'

class AgregarEquipo extends Component {

  componentDidMount(){
    axios
      .get('http://localhost:8000/listar/listar_programas/')
      .then((response) => { 
        console.log(response);
        this.setState({ programas: response.data})
        })
      .catch(error => {
        console.log(error);
        });

    axios
      .get('http://localhost:8000/listar/listar_semestres/')
      .then((response) => { 
        this.setState({ semestres: response.data})
        })
      .catch(error => {
        console.log(error);
        });

    axios
      .get('http://localhost:8000/registro/listar_deportes/')
      .then((response) => { 
        //console.log(response);
        this.setState({ deportes: response.data})
        })
      .catch(error => {
        console.log(error);
        });
  }
     constructor(props) {
        super(props);
        this.state = {
            nombre_equipo: '', siglas: '', programas:[], semestres:[], deportes:[]
        };
        this.handleChange = this.handleChange.bind(this);
    }
    handleChange = (e) => { this.setState({ [e.target.id]: e.target.value })};

    handleChangePrograma = (e) => { this.setState({ programa: e.target.value }) 
      console.log(e.target)
    };
    handleChangeSemestre = (e) => { this.setState({ semestre: e.target.value }) 
      console.log(e.target)
    };
    handleChangeDeporte = (e) => { this.setState({ deporte: e.target.value }) 
      console.log(e.target)
    };
    handleSubmit = (e) => {

        e.preventDefault();
        //console.log(this.state);

        let form_data = new FormData();

        form_data.append('nombre_equipo', this.state.nombre_equipo);
        form_data.append('siglas', this.state.siglas);
        form_data.append('deporte', this.state.deporte);
        form_data.append('programa', this.state.programa);
        form_data.append('semestre', this.state.semestre);

        let url = 'http://localhost:8000/registro/reg_equipos/';
        axios.post(url, form_data, {
            headers: {
                'content-type': 'multipart/form-data'
            }
        })
        .then(res => {
          console.log(res.data);
          swal({ 
            title: "Sistema",
            text: "Equipo almacenado satisfactoriamente",
            icon: "success",
            timer: "3000"
            })
        })
        .catch(err => console.log(err))
   };
  render() {
  return (


    <div className=" col bg-info text-white d-flex flex-column align-items-center">
      <div className="row">
        <h1>Agregar equipo</h1>
      </div>

      <div className="row m-4">

        <form onSubmit={this.handleSubmit}>
          <div className="form-group ">
            <label for="nombre_equipo">Nombre del equipo</label>
            <input type="text" className="form-control" id="nombre_equipo" value={this.state.nombre_equipo} 
            onChange={this.handleChange} aria-describedby="emailHelp" placeholder="Nombre del equipo" />
          </div>

          <div className="form-group">
            <label for="siglas">Siglas</label>
            <input type="text" className="form-control" id="siglas" value={this.state.siglas} 
            onChange={this.handleChange} placeholder="Siglas" />
          </div>

          <div className="form-group ">
            <p6>Programa</p6>

            <select id="programa" name="programa" value={this.state.value} 
            onChange={this.handleChangePrograma}
            className="selectpicker form-control form-control-sm" data-live-search="true">
              <option></option>
              { this.state.programas.map(elemento =>(
                    <option key={elemento.id} value={elemento.id}> {elemento.nombre_programa} </option>
                  )
                )}
            </select>

          </div>

          <div className="form-group ">
            <p6>Semestre</p6>

            <select id="semestre" name="semestre" value={this.state.value} 
            onChange={this.handleChangeSemestre}
            className="react-select2-wrapper form-control form-control-sm" data-live-search="true">
              <option></option>
              { this.state.semestres.map(elemento =>(
                    <option value={elemento.id}> {elemento.nombre_semestre} </option>
                  )
                )}
            </select>

          </div>

          <div className="form-group ">
            <p6>Deporte</p6>

            <select id="deporte" name="deporte" value={this.state.value} 
            onChange={this.handleChangeDeporte}
            className="selectpicker form-control form-control-sm" data-live-search="true">
              <option></option>
              { this.state.deportes.map(elemento =>(
                    <option key={elemento.id} value={elemento.id}> {elemento.nombre_deporte} </option>
                  )
                )}
            </select>

          </div>

          <input type='submit' className='btn btn-primary' />
        </form>

      </div>
      
      <div class="row ">
        <div class="col">

            <ul class="nav">
            <li><Link to="Organizador"> Volver</Link></li>
            </ul>

        </div>
      </div>




    </div>



  );
  }
}

export default AgregarEquipo;