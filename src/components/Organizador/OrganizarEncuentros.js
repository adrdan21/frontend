import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import TimeField from 'react-simple-timefield';
import img_stilo from '../css/img_stilo/hora.css'

class OrganizarEncuentros extends Component {
   componentDidMount(){
        //Lista Equiopos
         axios
            .get('http://localhost:8000/registro/reg_equipos/')
            .then((response) => { 
               console.log(response);
               this.setState({ equipos: response.data})
            })
            .catch(error => {
               console.log(error);
            });
        //Deporte
        axios
            .get('http://localhost:8000/registro/listar_deportes/')
            .then((response) => { 
               console.log(response);
               this.setState({ deportes: response.data})
            })
            .catch(error => {
               console.log(error);
            });
   }

   constructor(props) {
        super(props);
        this.state = {
            fecha: '', hora: '', juez_1: '', juez_2: '', juez_3: '', equipos:[], deportes:[]
        };
        this.handleChange = this.handleChange.bind(this);
    }

   handleChange = (e) => { this.setState({ [e.target.id]: e.target.value })};

   handleChangeEquipo1 = (e) => { this.setState({ equipo1: e.target.value }) 
      console.log(e.target)
   };
   handleChangeDeporte = (e) => { this.setState({ deporte: e.target.value }) 
      console.log(e.target)
   };

   handleSubmit = (e) => {
        e.preventDefault();
        let form_data = new FormData();

        form_data.append('fecha', this.state.fecha);
        form_data.append('hora', this.state.hora);
        form_data.append('juez_1', this.state.juez_1);
        form_data.append('juez_2', this.state.juez_2);
        form_data.append('juez_3', this.state.juez_3);
        form_data.append('equipo1', this.state.equipo1);
        form_data.append('deporte', this.state.deporte);
        
        let url = 'http://localhost:8000/registro/organizar_encuentros/';
        axios.post(url, form_data, {
            headers: {
                'content-type': 'multipart/form-data'
            }
        })
        .then(res => {
          console.log(res.data);
        })
        .catch(err => console.log(err))
    };

    render() {
        return (
            <div class=" col bg-info text-white d-flex flex-column align-items-center">
                <div class="row">
                    <h1>Organizar Encuentro</h1>
                </div>
                <div class="row m-4">
                    <form onSubmit={this.handleSubmit}>
                        <div class="form-group ">
                            <label for="fecha">Fecha</label>
                            <input type="date" class="form-control" id="fecha" placeholder="fecha" />
                        </div>
                        <div class="form-group ">
                            <label for="hora">Hora</label>
                            <input type="time" class="form-control" id="hora" ></input>
                        </div>
                        <div class="form-group ">
                            <input type="text" class="form-control" id="juez_1" value={this.state.juez_1}
                            onChange={this.handleChange} placeholder="Juez 1" />
                        </div>
                        <div class="form-group ">
                            <input type="text" class="form-control" id="juez_2" value={this.state.juez_2} 
                            onChange={this.handleChange} placeholder="Juez 2" />
                        </div>
                        <div class="form-group ">
                            <input type="text" class="form-control" id="juez_3" value={this.state.juez_3} 
                            onChange={this.handleChange} placeholder="Juez 3" />
                        </div>

                        <div class="form-group ">
                            <label for="equipo1">Equipo 1</label>
                            <select id="equipo1" name="equipo1" value={this.state.value} 
                                onChange={this.handleChangeEquipo1}
                                className="selectpicker form-control form-control-sm" data-live-search="true">
                                <option></option>
                                { this.state.equipos.map(elemento =>(
                                <option key={elemento.id} value={elemento.id}> 
                                {elemento.nombre_equipo} </option>
                                ))}
                            </select>
                        </div>
                        <div class="form-group ">
                            <label for="deporte">Deporte</label>
                            <select id="deporte" name="deporte" value={this.state.value} 
                                onChange={this.handleChangeDeporte}
                                className="selectpicker form-control form-control-sm" data-live-search="true">
                                <option></option>
                                { this.state.deportes.map(elemento =>(
                                <option key={elemento.id} value={elemento.id}> 
                                {elemento.nombre_deporte}</option>
                                ))}
                            </select>
                        </div>
                        <button type="submit" class="btn btn-success">Registrar</button>
                    </form>
                </div>
                <li class="btn btn-primary"><Link to="Organizador" style={{color: 'white'}}> Cancelar</Link></li>
            </div>
            
        );
    }   
}

export default OrganizarEncuentros;