import React, { Component } from 'react';
import DjangoCSRFToken from 'django-react-csrftoken'
import { AlertProvider } from 'react-alerts-plus';
import swal from 'sweetalert'
import axios from 'axios';
import { useCookies } from 'react-cookie';



class Login extends Component {

  constructor(argumentos) {
    super(argumentos);

    this.state = {
      username: "", password: "", showing: true
    }

    this.onChange = this.onChange.bind(this);
    this.enviarDatosLogin = this.enviarDatosLogin.bind(this);
    this.loginout = this.loginout.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(showing){
    this.setState({ showing });
  }

  onChange(evento) {

    let name = evento.target.name;
    let value = evento.target.value;

    this.setState({
      [name]: value, //podría poner cada uno de las variables nombre1, nombre2, etc pero con esto me ahorro código
    });

    console.log(this.state);

  }

  enviarDatosLogin(evento) {

    evento.preventDefault();
    let form_data = new FormData();

        form_data.append('username', this.state.username);
        form_data.append('password', this.state.password);

    let url = 'http://localhost:8000/api-auth/';
        axios.post(url, form_data, {
            headers: {
              'content-type': 'application/json'
            }
        })
        .then(res => {
          this.handleChange(false)
          swal({ 
            title: "Sistema",
            text: "Bienvenido a Deportes ITP",
            icon: "success",
            timer: "3000"
            })
        })
        .catch(err => 
          swal({ 
            title: "Sistema",
            text: "Error de Credenciales",
            icon: "info",
            timer: "3000"
            })
          )

  }

  loginout(){
    axios
      .get('http://localhost:8000/logout/')
      .then((response) => { 
        console.log(response);
        swal({ 
            title: "Sistema",
            text: "Vuelve pronto a Deportes ITP",
            icon: "success",
            timer: "3000"
            })
        })
      .catch(error => {
        console.log(error);
        });
  }



  render() {
    const { showing } = this.state;
    return (

      <div className="container">
      { showing ? 
        <form action="" onSubmit={this.enviarDatosLogin} className="form-row  col-12 mt-2 " >
          <div className="form-col form-group  ">
            <div className="form-row row-3">
              <div className="form-group col-5 ">
                <input name="username" id="username" type="text"  value={this.state.username} onChange={this.onChange}
                 className="form-control form-control-sm" placeholder="username" required/>
              </div>
              <div className="form-group col-5 ">
                <input name="password" id="password"  value={this.state.password} onChange={this.onChange}
                 type="password" className="form-control form-control-sm" placeholder="Contraseña" required />
              </div>
              <div className="form-group col ">
                <input type="submit" value="Enviar" className="btn btn-outline-success btn-sm form-control form-control-sm" />
              </div>
            </div>
          </div>
        </form>
        : 
          <div className="form-group col-3 " >
            <button type="button" onClick={() => this.handleChange(true)}
              className="btn btn-outline-danger btn-sm form-control form-control-sm">
              Salir
            </button>
          </div>

        }
      </div>
    );
  }

}

export default Login;