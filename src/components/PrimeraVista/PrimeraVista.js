import React from 'react';
import Encabezado from '../Encabezado/Encabezado.js';
import PrimeraVistaCuerpo from './PrimeraVistaCuerpo.js';

function Puente() {

  return (

    <div className="container ">
      <Encabezado />
      <PrimeraVistaCuerpo />
    </div>

  );
}

export default Puente;