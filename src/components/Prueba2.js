import React, { Fragment, useState, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import axios from 'axios';



const FormularioProvicional = () => {

    const { register, errors, handleSubmit } = useForm();

    const [entradas, setentradas] = useState([]);


   /* const procesarFormulario = (data, e) => {
        console.log(data);
        setentradas([
            ...entradas,
            data
            
        ])
        // limpiar campos
        e.target.reset();
       
    }*/

    const submit = (data, e) => {

        console.log(data);
        setentradas([
            ...entradas,
            data
        ])
        // limpiar campos
        e.target.reset();

        data.preventDefault()
        fetch('http://localhost:8000/api/usuario/', {
          method: 'POST',
          body: JSON.stringify({ entradas }),
          headers: { 'Content-Type': 'application/json' },
        })
          .then(res => res.json())
          .then(json => setentradas(json.entradas))
         
      }

    


    return (
        <Fragment>
            <h4>Inscribirse</h4>
            <form onSubmit={handleSubmit(submit)}>

                <input name="identificacion" id="identificacion" ref={register({ required: { value: true, message: 'Ingrese número documento' } })}
                    className="form-control my-2"
                    placeholder="Ingrese El número  documento"
                ></input>
                <span className="text-danger text-small d-block mb-2"> {errors?.identificacion?.message}  </span>

                <input name="tipo_doc" id="tipo_doc" ref={register({ required: { value: true, message: 'Ingrese tipo de documento' } })}
                    className="form-control my-2" placeholder="Ingrese El tipo documento"></input>
                <span className="text-danger text-small d-block mb-2"> {errors?.tipo_doc?.message} </span>

                <input name="nombre1" id="nombre1" ref={register({ required: { value: true, message: 'Ingrese nombre 1' } })}
                    className="form-control my-2" placeholder="Ingrese nombre 1" ></input>
                <span className="text-danger text-small d-block mb-2"> {errors?.nombre1?.message} </span>


                <input name="nombre2" id="nombre2" ref={register({ required: { value: true, message: 'Ingrese descripción' } })}
                    className="form-control my-2" placeholder="Ingrese nombre 2" ></input>
                <span className="text-danger text-small d-block mb-2"> {errors?.nombre2?.message} </span>

                <input name="apellido1" id="apellido1" ref={register({ required: { value: true, message: 'Ingrese apellido 1' } })}
                    className="form-control my-2" placeholder="Ingrese apellido 1" ></input>
                <span className="text-danger text-small d-block mb-2"> {errors?.apellido1?.message} </span>


                <input name="apellido2" id="apellido2" ref={register({ required: { value: true, message: 'Ingrese apellido 2' } })}
                    className="form-control my-2" placeholder="Ingrese apellido 2" ></input>
                <span className="text-danger text-small d-block mb-2"> {errors?.apellido2?.message} </span>

                <input name="email" id="email" ref={register({ required: { value: true, message: 'Ingrese email' } })}
                    className="form-control my-2" placeholder="Ingrese email" ></input>
                <span className="text-danger text-small d-block mb-2"> {errors?.email?.message} </span>

                <input name="celular" id="celular" ref={register({ required: { value: true, message: 'Ingrese celular' } })}
                    className="form-control my-2" placeholder="Ingrese celular" ></input>
                <span className="text-danger text-small d-block mb-2"> {errors?.celular?.message} </span>

                <input name="fecha_nac" id="fecha_nac" ref={register({ required: { value: true, message: 'Ingrese fecha nacimiento' } })}
                    className="form-control my-2" placeholder="Ingrese fecha nacimiento" ></input>
                <span className="text-danger text-small d-block mb-2"> {errors?.fecha_nac?.message} </span>

                <input name="genero" id="genero" ref={register({ required: { value: true, message: 'Ingrese  genero' } })}
                    className="form-control my-2" placeholder="Ingrese genero" ></input>
                <span className="text-danger text-small d-block mb-2"> {errors?.genero?.message} </span>

                <input name="username" id="username" ref={register({ required: { value: true, message: 'Ingrese  nombre de usuario' } })}
                    className="form-control my-2" placeholder="Ingrese usuario" ></input>
                <span className="text-danger text-small d-block mb-2"> {errors?.username?.message} </span>

                <input name="password" id="password" ref={register({ required: { value: true, message: 'Ingrese  su contraseña' } })}
                    className="form-control my-2" placeholder="Ingrese contraseña" ></input>
                <span className="text-danger text-small d-block mb-2"> {errors?.password?.message} </span>




                <button type="submit" className="btn btn-primary">  Agregar  </button>


            </form>

            
        </Fragment>
    );
}

export default FormularioProvicional;

/**
 * 
 * <ul className="mt-2">
                {
                    entradas.map((item, index) =>
                        <li key={index}>

                            -{item.identificacion} <br/>
                            -{item.tipo_doc}<br/>
                            -{item.nombre1}<br/>
                            -{item.nombre2}<br/>
                            -{item.apellido1}<br/>
                            -{item.apellido2}<br/>
                            -{item.email}<br/>
                            -{item.celular}<br />
                            -{item.fecha_nac}<br />
                            -{item.genero}<br />
                            -{item.username}<br />
                            -{item.password}<br />
                        </li>
                    )
                }
            </ul>
 * 
 */